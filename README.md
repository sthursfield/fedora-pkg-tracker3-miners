This repository contains packaging instructions for Tracker Miners 3 on Fedora.
These are unofficial and were created by Sam Thursfield during the development
of Tracker 3 as a way of testing it before the final release.

The packaging is based on https://src.fedoraproject.org/rpms/tracker-miners/
